import turtle

#assign background color
turtle.bgcolor("black")

#create turtle
tt = turtle.Turtle()
tt.speed(1)
tt.pensize(1)



#create base quadilateral class
class Quad():
    def __init__(self,l1,l2,l3,l4):
        self.s1 = l1
        self.s2 = l2
        self.s3 = l3
        self.s4 = l4
        self.an1 = 45
        self.an2 = 90
        self.an3 = 120
    title = "Quadilateral"

    def shape(self):
        print(self.title)

    def show(self):
        tt.clear()
        tt.write(self.title)
        tt.goto(self.s1, 0)
        tt.pendown()
        tt.color("white")
        tt.forward(self.s2)
        tt.left(self.an1)
        tt.forward(self.s3)
        tt.left(self.an2)
        tt.forward(self.s4)
        tt.left(self.an3)
        tt.goto(self.s1, 0)
        tt.penup()
#entry as side, side, side
q = Quad(45,80,65,70)
q.show()


#create Kite from Quadilateral
class Kite(Quad):
    def __init__(self,l1,l2,l3,l4):
        super().__init__(l1,l2,l3,l4)
        self.an1 = 85
        self.an2 = 90
        self.an3 = 85
    title = "Kite"

    def parents(self):
        print("Parents of Kite:",Quad.title)#will edit this later
#entry as bottom left, bottom right, top right, top left
k = Kite(60,60,40,40)
k.show()
k.parents()



#create trapezoid class from base Quadilateral
class Trap(Quad):
    def __init__(self,l1,l2,l3,l4):
        super().__init__(l1,l2,l3,l4)
        self.an1 = 60
        self.an2 = 85
        self.an3 = 30
    title = "Trapezoid"
    def parents(self):
        print("Parents of Trapezoid:", Quad.title)
#entry as base, right height, top, left height
t = Trap(100,80,75,60)
t.show()
t.parents()



#create paralleogram from Trapezoid
class Para(Trap):
    def __init__(self,l1,l2,l3,l4):
        Trap.__init__(self,l1,l2,l3,l4)
        self.an1 = 35
        self.an2 = 145
        self.an3 = 35
    title = "Parallelogram"
    def parents(self):
        print("Parents of Parallelogram:", Trap.title)
#entry as side, base, side, top
p = Para(60,80,60,80)
p.show()
p.parents()




#create rhombus with Kite as parent
class Rhombus(Kite, Para):
    def __init__(self, l1, l2, l3, l4):
        Kite.__init__(self,l1, l2, l3, l4)
        self.an1 = 106
        self.an2 = 74
    title = "Rhombus"
    def parents(self):
        print("Parents of Rhombus:",Kite.title + ",",Para.title)
#entry as side, side, side
r = Rhombus(60,60,60,60)
r.show()
r.parents()



#create isosceles Trap from Trapezoid
class IsoT(Trap):
    def __init__(self,l1,l2,l3,l4):
        Trap.__init__(self,l1,l2,l3,l4)
        self.an1 = 129
        self.an2 = 51
        self.an3 = 51
    title = "Isosceles Trapezoid"
    def parents(self):
        print("Parents of Iso. Trapezoid: " + Trap.title)
#entry as height, base, top, height
i = IsoT(70,150,60,70)
i.show()
i.parents()



#rectangle from Isoscelese Trapezoid
class Rect(IsoT, Para):
    def __init__(self,l1,l2,l3,l4):
        IsoT.__init__(self,l1,l2,l3,l4)
        self.an1 = 90
        self.an2 = 90
        self.an3 = 90
    title = "Rectangle"
    def parents(self):
        print("Parents of Rectangle:",Para.title+",", IsoT.title)
#entry as height1, base, height2, top
r2 = Rect(60,100,60,100)
r2.show()
r2.parents()



#create square from Rhombus
class Square(Rhombus, Rect):
    def __init__(self, l1,l2,l3,l4):
        Rhombus.__init__(self,l1,l2,l3,l4)
        self.an1 = 90
        self.an2 = 90
        self.an3 = 90
    title = "Square"
    def parents(self):
        print("Parents of Square:",Rhombus.title+",", Rect.title)
#entry as side, side, side
s = Square(60,60,60,60)
s.show()
s.parents()
